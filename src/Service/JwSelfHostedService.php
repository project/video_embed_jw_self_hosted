<?php

namespace Drupal\video_embed_jw_self_hosted\Service;

use BotrAPI;
use Drupal\Core\Url;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * The JW Player (self hosted) service class.
 *
 * @package video_embed_field
 * @deprecated Use SystemUserManager instead
 */
class JwSelfHostedService {

  /**
   * The remote API path to fetch the players.
   *
   * @var string
   */
  const PATH_LIST_PLAYERS = "/players/list";

  /**
   * The remote API path to fetch all templates.
   *
   * @var string
   */
  const PATH_LIST_TEMPLATES = "/accounts/templates/list";

  /**
   * The default order to sort the items by.
   *
   * By default it's sorted ascending by name.
   *
   * @var string
   */
  const ORDER_DEFAULT = "name:asc";

  /**
   * The status code got by the botr api.
   *
   * @var string
   */
  const RESPONSE_STATUS_OK = "ok";

  /**
   * The default expiration duration of a media.
   *
   * In general used for building the expiration and signature queries.
   * Only used if there are no settings available.
   *
   * @var int
   */
  const EXPIRY = 3600;

  /**
   * A new botrapi instance.
   *
   * @var \BotrAPI
   */
  protected $botr;

  /**
   * The drupal configuration interface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $config;

  /**
   * The Cache Backend Interface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * JwService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   */
  public function __construct(ConfigFactoryInterface $config_factory, CacheBackendInterface $cache_backend) {
    $this->config = $config_factory->get('video_embed_jw_self_hosted.settings');
    $this->botr = new BotrAPI($this->config->get('key'), $this->config->get('secret'));
    $this->cacheBackend = $cache_backend;
  }

  /**
   * Returns all players configured within the jw player account.
   *
   * @return array
   *   All remote players
   */
  public function getPlayers() {
    $cid = $this->buildCacheIdByContext('players');
    $players = [];

    if ($cache = $this->cacheBackend->get($cid)) {
      $players += $cache->data;
    }
    else {
      $response = $this->botr->call($this::PATH_LIST_PLAYERS, ['order_by' => $this::ORDER_DEFAULT]);
      if ($response['status'] == $this::RESPONSE_STATUS_OK && isset($response['players']) && !empty($response['players'])) {
        foreach ($response['players'] as $delta => $player) {
          $players[$player['key']] = $player['name'];
        }
        $this->cacheBackend->set($cid, $players, Cache::PERMANENT, ['jw_self_hosted', 'jw_self_hosted:players']);
      }
    }

    return $players;
  }

  /**
   * Returns all templates (media formats) from the jw player website account.
   *
   * @return array
   *   All remote JW Player templates.
   */
  public function getTemplates() {
    $templates = [];
    $cid = $this->buildCacheIdByContext('templates');

    if ($cache = $this->cacheBackend->get($cid)) {
      $templates += $cache->data;
    }
    else {
      $response = $this->botr->call($this::PATH_LIST_TEMPLATES, ['order_by' => $this::ORDER_DEFAULT]);
      if ($response['status'] == $this::RESPONSE_STATUS_OK && isset($response['templates']) && !empty($response['templates'])) {
        foreach ($response['templates'] as $delta => $template) {
          $templates[$template['key']] = $template['name'];
        }
        $this->cacheBackend->set($cid, $templates, Cache::PERMANENT, ['jw_self_hosted', 'jw_self_hosted:templates']);
      }
    }

    return $templates;
  }

  /**
   * Get the access expiration duration of videos.
   *
   * In general used for building the jw url signature query.
   *
   * @return int
   *   The expiry timestamp
   */
  public function getExpiry() {
    $now = time();

    if (empty($this->config->get('expiry'))) {
      return $now + $this::EXPIRY;
    }

    return (int) $now + $this->config->get('expiry');
  }

  /**
   * Returns the signed url.
   *
   * @param string $url
   *   The base url e.g. 'https://content.jwplatform.com/'.
   *   Please provide a trailing slash!
   * @param string $path
   *   The path of the jw player hosted content.
   * @param string $extension
   *   The file extension of the media e.g. '.mp4'.
   *
   * @return \Drupal\Core\GeneratedUrl
   *   The public signed url
   */
  public function buildSignedUrl($url, $path, $extension) {
    $signature = md5($path . $extension . ':' . $this->getExpiry() . ':' . $this->config->get('secret'));
    $query = ['exp' => $this->getExpiry(), 'sig' => $signature];

    return Url::fromUri($url . $path . $extension, ['query' => $query])->toString();
  }

  /**
   * Returns the url for media thumbnails.
   *
   * @param string $mediaId
   *   The media Id to get the thumbnail from.
   * @param int $width
   *   The width of the thumbnail.
   *
   * @return string
   *   The remote thumbnail url.
   */
  public function buildThumbnailUrl($mediaId, $width) {
    return sprintf("https://cdn.jwplayer.com/thumbs/%s-%s.jpg", $mediaId, $width);
  }

  /**
   * Builds the cache id.
   *
   * @param string $context
   *   The context of the cache id to build for.
   *
   * @return string
   *   The cache ID or empty string.
   */
  public function buildCacheIdByContext(string $context = '') {
    if (!empty($context)) {
      return "jw_self_hosted:{$this->config->get('key')}:{$context}";
    }

    return '';
  }

}
