<?php

namespace Drupal\video_embed_jw_self_hosted\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\video_embed_field\Plugin\Field\FieldFormatter\Video;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\video_embed_field\ProviderManagerInterface;

/**
 * Plugin implementation of the video field formatter.
 *
 * @FieldFormatter(
 *   id = "video_embed_field_video",
 *   label = @Translation("Video"),
 *   field_types = {
 *     "video_embed_field"
 *   }
 * )
 */
class VideoJwSelfHosted extends Video {

  /**
   * The JW Player (self hosted) Service.
   *
   * @var \Drupal\video_embed_jw_self_hosted\Service\JwSelfHostedService
   */
  protected $jw;

  /**
   * VideoJwSelfHosted constructor.
   *
   * {@inheritdoc}
   */
  public function __construct(string $plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, string $label, string $view_mode, array $third_party_settings, ProviderManagerInterface $provider_manager, AccountInterface $current_user) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $provider_manager, $current_user);
    $this->jw = \Drupal::service('jw_self_hosted.service');
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $default_settings = parent::defaultSettings();

    return $default_settings + [
      'jw_player' => '',
      'jw_template' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = parent::viewElements($items, $langcode);
    $settings = $items->getSettings();
    $allowed_providers = empty($settings['allowed_providers']) ? [] : $settings['allowed_providers'];
    if (in_array('jw_self_hosted', array_values($allowed_providers))) {
      foreach ($items as $delta => $item) {
        $provider = $this->providerManager->loadProviderFromInput($item->value, $allowed_providers);

        if (!$provider) {
          $element[$delta] = ['#theme' => 'video_embed_field_missing_provider'];
        }
        else {
          $player = $this->getSetting('jw_player');
          $template = $this->getSetting('jw_template');
          $autoplay = $this->currentUser->hasPermission('never autoplay videos') ? FALSE : $this->getSetting('autoplay');
          $element[$delta] = $provider->renderEmbedCode($this->getSetting('width'), $this->getSetting('height'), $autoplay, $player, $template);
          $element[$delta]['#cache']['contexts'][] = 'user.permissions';

          $element[$delta] = [
            '#type' => 'container',
            '#attributes' => ['class' => [Html::cleanCssIdentifier(sprintf('video-embed-field-provider-%s', $provider->getPluginId()))]],
            'children' => $element[$delta],
          ];

          // For responsive videos, wrap each field item in it's own container.
          if ($this->getSetting('responsive')) {
            $element[$delta]['#attached']['library'][] = 'video_embed_field/responsive-video';
            $element[$delta]['#attributes']['class'][] = 'video-embed-field-responsive-video';
          }
        }

      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);
    $allowed_providers = $this->getFieldSetting('allowed_providers');

    if (in_array('jw_self_hosted', array_values($allowed_providers))) {
      $elements['jw_player'] = [
        '#type' => 'select',
        '#title' => t('JW Player'),
        '#options' => $this->jw->getPlayers(),
        '#default_value' => $this->getSetting('jw_player'),
      ];

      $elements['jw_template'] = [
        '#type' => 'select',
        '#title' => t('JW Template'),
        '#options' => $this->jw->getTemplates(),
        '#default_value' => $this->getSetting('jw_template'),
      ];
    }

    return $elements;
  }

}
