(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.video_embed_jw_self_hosted = {
    attach: function (context, settings) {
      const elements = once('video_embed_jw_self_hosted', '[id^="jw-self-hosted-"]', context);
      elements.forEach(function(element){
        settings.la_jw = settings.la_jw || {};
        settings.la_jw.players = settings.la_jw.players || [];
        settings.la_jw.players.push(
          jwplayer(element.id).setup({
            'playlist':  'https://cdn.jwplayer.com/v2/media/' + element.dataset.media
          })
        );
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
